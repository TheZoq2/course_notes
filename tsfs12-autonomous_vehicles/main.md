---
header-includes: |
    \usepackage{tikz}
---
# Discrete optimisation, Graph search

## Notation

- $X$ state space
    - $X_i$ and $X_g$ are start and end states
- $U(X)$ action space.
- $f(X, U)$ state transitions. ($X'$ is result)
    - result of taking action $U(X)$ in state $X$


## A\*: Dijkstra with heuristics

Augment dijkstra with a heuristic $h(x)$ to improve performance.

If heuristic is always an under-estimation of the goal cost, ($h^*(x) \leq h(x)$
where $h*$ is the true cost), it is **admissible** which guarantees an optimal
solution.

Further, a heuristic is **consistent** if it always improves as you get closer to
the goal, i.e. $h(x) - h(x') \leq d(x, x')$ where $d(x, x')$ is the distance
between $x$ and $x'$.

A consistent heuristic avoids adding nodes to the queue more than once.

## Any-time planning.

A large amount of underestimation of the cost results in longer computation time. A slight over-estimation might not reduce the cost much but make computation much faster.

### ARA\*

ARA\* uses this to guarantee *some* results from A\*. The heuristic is modified
with $h_c(x)$ = $c \cdot h_(x)$. The algorithm is first run with a large $c$
which is iteratively reduced. Large parts of the computation can be re-used
when doing this. Once time runs out, or an optimal path has been found, the
result is returned.

**Read the paper on this, see slides**


## Search direction

A* does not have to be run in the "forward" direction, it can also be used for
backwards, or bi-directional search. This can be advantageous if finding the
"entrance" to a region is easy from one direciton, but not from another, for
example.


# Motion planning

A differentially driven robot, i.e. one where wheels on either side are
controlled independently can be described using three states: The x-position
$x$, the y-position $y$, and the angle $\theta$.

It is controlled by two inputs $u_1$ and $u_2$ which drive the two different
wheels. Since the number of inputs is less than the number of states, the robot
is *under actuated*.

The model for the vehicle can be described as

$$
q = \begin{pmatrix}
        x \\
        y \\
        z
    \end{pmatrix},
u = \begin{pmatrix}
    u_1 \\
    u_2
\end{pmatrix}
$$

And the velocity through the state space $\dot{q}$ is a function of the state and
input

$$
\dot{q} = f(q, u) = u_1 \cdot f_1(q) + u_2 \cdot f_2(q) \space
,\quad f_1(q) = \begin{pmatrix}
    0 \\
    0 \\
    1
\end{pmatrix}
,\quad f_2(q) = \begin{pmatrix}
    cos(\theta) \\
    sin(\theta) \\
    0
\end{pmatrix}
$$

That is, $f_1$ and $f_2$ descrbe the change in the state space when $u_1$ or
$u_2$ are applied

While the robot can not directly move in the y-direction, it can do so either
by turning and then moving forward, or if space is limited, zig zag forward and
backwards.

```
{maybe add a figure here}
```

The zig zag pattern can be described as a sequence of inputs in the following way

$$
u = \big[
\begin{pmatrix}
-1 \\ 0
\end{pmatrix}
\begin{pmatrix}
0 \\ 1
\end{pmatrix}
\begin{pmatrix}
1 \\ 0
\end{pmatrix}
\begin{pmatrix}
0 \\ -1
\end{pmatrix}
\big]
$$


In general, the result of applying multiple inputs over time are described by the
jacobian, this is probably described better in the modern robotics course notes.

## Holynomic and non-holynomic systems

The example robot above is non-holynomic 

```
Read up on this
```

## Car with front wheel steering

First of all, the model depends on the use case, there is no general one size
fits all model.

A simple (kinematic, meaning it has no forces) model of a car is given in the
figure below:

Where the state space consists of $x$, $y$, and $theta$. The derivatives are

$$
\dot{x} = v \cos(\theta), \quad
\dot{y} = v \sin(\theta), \quad
\dot{\theta} = v \frac{tan(\delta)}{l}
$$

We also need to specify control inputs and their effects inputs. There are two inputs,
$u_1$ and $u_2$. I.e., u_1 controls the steering, in particular $tan(u_1) = \delta$

$$
\dot{\theta} = v \frac{u_1}{l}, \quad
\dot{v} = u_2, \quad
u_1 \in [min, max]
$$

However, this will lead to a steering angle that can change instantly which is
unrealistic and uncomfortable for passengers. Therefore, $dot{\delta}$ is used
as an input instead with $\delta$ being made into a state.




# Modelling of Aerial and Water vehicles

## Modelling and control of quad-rotors

Orientation is given by rotation matrix `R`. Position is denoted by `\xi`

Motion equation given by slide 13 (forces and torques)

## Water vehicles

- Forward velocity: surge
- Sideway velocity: Sway
- Up-Down velocity: Have

Water adds additional inertia compared to air vehicles.




# Path trajectory following

Basic idea is standard error based controller

```
Trajectory --(+)-->[controller]-->[Vehicle]----Position
           ^                              |
           *------------------------------*
```



## Paths and trajectories

Path is a curve in space. It has no time information. A trajectory has a time
stamp associated with each point.

Paths parametrized by position, trajectories by time

Path following is easier than trajectory following

Trajectory control can be split into acceleration and steering as separate
steps. 

Trajectory control is important when avoiding collisions with other vehicles,
for example

### Paths

$$
P(s) = (X(s), Y(s))
$$

Any point along a path has a tangent and a normal vector. The curvature of the path is the rate of turning of the tangent against the normal vector

$$
dT(s)/ds = k(s)N(s)
$$

In the global coordinate system, the Tangent vector is

$$
T=(cos(\theta), sin(\theta))^T
$$

$$
n=(-sin(\theta), cos(\theta))^T
$$


Curvature:

$$
dT/ds =
[sin(theta) \cdot d\theta/ds, cos(theta) \cdot d\theta/ds] = d\theta/ds N(s)
$$

In a circle $d\theta/ds = d\theta/(r\cdot d\theta) = 1/r$

### How to compute the curvature if you only have $(X(s), Y(s))$?

(see slide 16). Explicit formula exists



### Curvature of single track kinematic model

$$
1/l \cdot tan(\delta)
$$

^^ This is used as control signal $u$

$$
K = d\theta/ds = d\theta/dt \cdot dt/ds
$$

$$
dt/ds = 1/v
$$


## Pure pursuit control

Vehicle is somewhere, heading somewhere else. Not on path


Chose some look ahead horizon ($l$). Draw a circle around the current position.

Aim for intersection of path and look-ahead circle $(x,y)$ <- in local
coordinate system of vehicle.

How to chose steering angle?. Want radius of circle to follow, that gives
steering angle $u$

Want circle segment that is tangent to current position and velocity vector and
intersets $(x,y)$. That circle has radius $r$

(See photo of board)

Know that $x^2+y^2=l^2$ and $r^2 + d^2 = y^2$. $x=r+d$

Equation system ^

Results in $l^2/2 \cdot x$


Finally, steering angle choice is $tan(\delta) = L \cdot 2x/l^2$

Has problems:
- For example,what to do if further away from path than $l$

Properties:
- Short horizon -> agile but rough
- Long horizon -> smooth but less agule
- Possible extensions:
    - Ellipses instead of circles
    - Offset circle (?)
    - Speed dependence



## Path tracking as a state stabilization problem

Two errors: distance from path $d$, heading error $\theta_e$. Make them go to
zero.

To use control framework, we want $\dot{d}$ and $\dot{\theta_e}$ which is the
frénet frame

Slides p27


### Curvature $C(s)$

$$
c(s) = d \theta_s / ds = d\theta_ss / dt \cdot dt/ds
    \leftarrow \dot{\theta_s} = \dot{s} \cdot c(s)
$$

$\theta_s$ is curvature of the path

$dt/ds$ is 1 over path speed


Takeaway:

$$
\dot{d} = v sin(\theta_e)
\dot{\theta_e} = v u \dot{s} c(s)
\dot{s} \frac{v_cos(\theta_e)}{I-c(s)d}
$$


# Collaborative control

How to make a swarm of robots work together.

The basic goal is for all robots to reach some position $p_i^*$ which can either
be global, or relative to other robots.


This leads to three classes of collaborative control:

 - *position based*: where the global position of each robot is known
 - *displacement based* where relative positions between robots is known
 - *distance based* where only the relative distance between robots is known

## Position based

When the global position of each of the robots is known, no communication is
*required*.  The robots can independently navigate to the correct position in
the formation. (assuming they avoid each other).

In a simple model, proportional control can be employed:

$$
u_i = k_p(p_i^* - p_i)
$$

If velocity is also controlled:

$$
u_i = kp(p_i^* - p_i) + k_v(v_i^* - v_i)
$$


## Displacement based

Each robot knows the orientation of its local coordinate system in the global
one, but not the position. It also knows the relative position of a subset of
the other robots. $p_i - p_j$

The position information that each robot has can be described by a directed
graph.  An arrow pointing *to* a node means that the pointed to node knows the
position of the pointer. I.e. the pointer is a neighbour of the pointee. The set
of neighbours is denoted by $\mathcal{N}_i$

A proportional control law can be written as

$$
u_i = \sum_{j \in \mathcal{N}_i} w_{ij} ((p_i-p_j) - (p_i^* - p_j^*))
$$

I.e. the current relative position minus the target relative position multiplied
by some weight $w$.

In some cases, some of the robots know their global position as well, in that case,
another term is added: $g_i (p_i - p_i^*)$.

A group of robots can only reach a target formation if the neighbour graph has
a spanning tree, as they all have (defered) knowlege of position relative to
some common point (the position of the robot at the root of the tree)

## Distance based formation control

The third class of collaborative control, here, the relative position isn't
known, nor is the relative orientation of the local coordinate system in the
global system. The only thing that is known is the distance between a robot and
some neighbours.

In this case, the neighbour graph is undirected.

The goal to fullfill is then to get the distance between each of the robots $d_{ij}$ to
the desired distance $||p_i^* - p_j^*||$

A formation can be non-rigid, minimally rigid or fully rigid.

In a non-rigid formation, there is no unique position in the world that corresponds to a
set of relative distances. As shown below

\begin{tikzpicture}
    \node[shape=circle,draw=black] (A) at (0,0) {};
    \node[shape=circle,draw=black] (B) at (2,0) {};
    \node[shape=circle,draw=black] (C) at (0,2) {};
    \node[shape=circle,draw=black] (D) at (2,2) {};

    \node[shape=circle,draw=black] (A_) at (5+1.4,0) {};
    \node[shape=circle,draw=black] (B_) at (5+3.4,0) {};
    \node[shape=circle,draw=black] (C_) at (5+0,1.4) {};
    \node[shape=circle,draw=black] (D_) at (5+2,1.4) {};

    \path [-] (A) edge (B);
    \path [-] (B) edge (D);
    \path [-] (C) edge (D);
    \path [-] (C) edge (A);

    \path [-] (A_) edge (B_);
    \path [-] (B_) edge (D_);
    \path [-] (C_) edge (D_);
    \path [-] (C_) edge (A_);
\end{tikzpicture}

A minimally rigid model does not have this problem, there are enough edges
to not allow movement outside of the desired pose. However, there are more
poses that satisfy the distance constraint, they just can't be reached without
pertrubations.

\begin{tikzpicture}
    \node[shape=circle,draw=black] (A) at (0,0) {};
    \node[shape=circle,draw=black] (B) at (2,0) {};
    \node[shape=circle,draw=black] (C) at (0,1.5) {};
    \node[shape=circle,draw=black] (D) at (2,1.5) {};

    \node[shape=circle,draw=black] (A_) at (3 + 0,0) {};
    \node[shape=circle,draw=black] (B_) at (3 + 2,0) {};
    \node[shape=circle,draw=black] (C_) at (3 + 0,1.5) {};
    \node[shape=circle,draw=black] (D_) at (3 + 0.7,-0.6) {};

    \path [-] (A) edge (B);
    \path [-] (B) edge (D);
    \path [-] (B) edge (C);
    \path [-] (C) edge (D);
    \path [-] (C) edge (A);

    \path [-] (A_) edge (B_);
    \path [-] (B_) edge (D_);
    \path [-] (B_) edge (C_);
    \path [-] (C_) edge (D_);
    \path [-] (C_) edge (A_);
\end{tikzpicture}


A 2d minimally-rigid formation always has 2N-6 edges, and can be constructed by
starting with a triangle and adding one node and two edges at a time.

The relative deformability of a minimally rigid graph can be solved with a fully rigid graph

\begin{tikzpicture}
    \node[shape=circle,draw=black] (A) at (0,0) {};
    \node[shape=circle,draw=black] (B) at (2,0) {};
    \node[shape=circle,draw=black] (C) at (0,1.5) {};
    \node[shape=circle,draw=black] (D) at (2,1.5) {};

    \path [-] (A) edge (B);
    \path [-] (B) edge (D);
    \path [-] (B) edge (C);
    \path [-] (C) edge (D);
    \path [-] (A) edge (D);
    \path [-] (C) edge (A);
\end{tikzpicture}
